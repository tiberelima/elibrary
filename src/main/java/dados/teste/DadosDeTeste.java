package dados.teste;

import controle.ControleDeEmprestimo;
import controle.ControleDeEspera;
import controle.ControleDeExemplares;
import controle.ControleDeLivro;
import entidade.Cliente;
import entidade.Emprestimo;
import entidade.Livro;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class DadosDeTeste implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) {

        Livro l1 = new Livro();
        l1.setAno("2005");
        l1.setAutor("Booch, G. et al.");
        l1.setEdicao("Segunda edição");
        l1.setEditora("Elsevier");
        l1.setGenero("TI");
        l1.setISBN("13 978-85-352-1784-1");
        l1.setTitulo("UML: guia do usuário");

        Livro l3 = new Livro();
        l3.setAno("2006");
        l3.setAutor("Bezerra, E.");
        l3.setEdicao("Terceira edição");
        l3.setEditora("Campus");
        l3.setGenero("TI");
        l3.setISBN("85-352-1032-6");
        l3.setTitulo("Princípios de análise e projeto de sistemas UML: Um guia prático para modelagem de sistemas");

        Livro l6 = new Livro();
        l6.setAno("2004");
        l6.setAutor("Lafore, R.");
        l6.setEdicao("Segunda edição");
        l6.setEditora("Ciência Moderna Ltda.");
        l6.setGenero("TI");
        l6.setISBN("85-7393-375-5");
        l6.setTitulo("Estruturas de dados & algoritmos em Java™");

        Livro l10 = new Livro();
        l10.setTitulo("Use a Cabeça Padrões de Projetos");
        l10.setAutor("Freeman, Elisabeth; Freeman, Eric");
        l10.setEditora("Alta Books");
        l10.setAno("2009");
        l10.setEdicao("Segunda edição");
        l10.setISBN("978-8-576-08174-6");
        l10.setGenero("TI");

        Livro l4 = new Livro();
        l4.setAno("2009");
        l4.setAutor("Puga, S; Rissetti G.");
        l4.setEdicao("Segunda edição");
        l4.setEditora("Pearson Prentice Hall");
        l4.setGenero("TI");
        l4.setISBN("978-85-7605-207-4");
        l4.setTitulo("Lógica de programação e estruturas de dados, com aplicações em Java");

        Livro l2 = new Livro();
        l2.setAno("2010");
        l2.setAutor("Ascencio, A. F. G.; Araujo, G. S.");
        l2.setEdicao("Primeira edição");
        l2.setEditora("Pearson Prentice Hall");
        l2.setGenero("TI");
        l2.setISBN("978-85-7605-881-6");
        l2.setTitulo("Estruturas de dados: algoritmos, análise da complexidade e implementações em JAVA e C/C++");

        Livro l5 = new Livro();
        l5.setAno("2005");
        l5.setAutor("Braude, E.");
        l5.setEdicao("Primeira edição");
        l5.setEditora("Bookman");
        l5.setGenero("TI");
        l5.setISBN("85-3630-493-6");
        l5.setTitulo("Projeto de software: da programação à arquitetura: uma abordagem baseada em Java");

        Livro l7 = new Livro();
        l7.setAno("2008");
        l7.setAutor("Koffman, E. B; Wolfgang, P. A. T.");
        l7.setEdicao("Primeira edição");
        l7.setEditora("LTC");
        l7.setGenero("TI");
        l7.setISBN("978-85-216-1603-0");
        l7.setTitulo("Objetos, abstração, estruturas de dados e projeto usando Java versão 5.0");

        Livro l8 = new Livro();
        l8.setTitulo("Introdução à arquitetura e design de software: uma visão sobre a plataforma java");
        l8.setAutor("Lopes, Sérgio et al.");
        l8.setEditora("Campus");
        l8.setAno("2012");
        l8.setEdicao("Primeira edição");
        l8.setISBN("978-85-352-5029-9");
        l8.setGenero("TI");

        Livro l9 = new Livro();
        l9.setTitulo("Java Generics and Collections");
        l9.setAutor("Naftaling, Maurice; Wadler, Philip");
        l9.setEditora("O'Reilly");
        l9.setAno("2006");
        l9.setEdicao("Primeira edição");
        l9.setISBN("978-0-596-52775-4");
        l9.setGenero("TI");

        Cliente c1 = new Cliente();
        c1.setNome("Aline Batista Rodrigues");
        c1.setRg("12.345.678-9");
        c1.setEmail("email@ficticio.com");
        c1.setTelefone("(11) 9 8765-4321");

        Cliente c2 = new Cliente();
        c2.setNome("Danilo Martins de Araújo");
        c2.setRg("12.345.678-9");
        c2.setEmail("email@ficticio.com");
        c2.setTelefone("(11) 9 8765-4321");

        Cliente c3 = new Cliente();
        c3.setNome("Francine dos Reis Sampaio");
        c3.setRg("12.345.678-9");
        c3.setEmail("email@ficticio.com");
        c3.setTelefone("(11) 9 8765-4321");

        Cliente c4 = new Cliente();
        c4.setNome("Guilherme Rocha");
        c4.setRg("12.345.678-9");
        c4.setEmail("email@ficticio.com");
        c4.setTelefone("(11) 9 8765-4321");

        Cliente c5 = new Cliente();
        c5.setNome("Isabela Duran Oliveira Souza");
        c5.setRg("12.345.678-9");
        c5.setEmail("email@ficticio.com");
        c5.setTelefone("(11) 9 8765-4321");

        Cliente c6 = new Cliente();
        c6.setNome("Leonardo Ciacareli");
        c6.setRg("12.345.678-9");
        c6.setEmail("email@ficticio.com");
        c6.setTelefone("(11) 9 8765-4321");

        ControleDeLivro controle = new ControleDeLivro();
        ControleDeEmprestimo controleDeEmprestimo = new ControleDeEmprestimo();
        ControleDeExemplares controleDeExemplares = new ControleDeExemplares();
        ControleDeEspera controleDeEspera = new ControleDeEspera();

        controle.adicionar(l1);
        controle.adicionar(l3);
        controle.adicionar(l6);
        controle.adicionar(l10);
        controle.adicionar(l4);
        controle.adicionar(l2);
        controle.adicionar(l5);
        controle.adicionar(l7);
        controle.adicionar(l8);
        controle.adicionar(l9);

        controleDeExemplares.adicionarExemplares(l1, 12);
        controleDeExemplares.adicionarExemplares(l3, 3);
        controleDeExemplares.adicionarExemplares(l6, 8);
        controleDeExemplares.adicionarExemplares(l10, 7);
        controleDeExemplares.adicionarExemplares(l4, 5);
        controleDeExemplares.adicionarExemplares(l2, 10);
        controleDeExemplares.adicionarExemplares(l5, 13);
        controleDeExemplares.adicionarExemplares(l7, 10);
        controleDeExemplares.adicionarExemplares(l8, 6);
        controleDeExemplares.adicionarExemplares(l9, 16);

        Emprestimo e1 = new Emprestimo(c1, l9);
        Emprestimo e2 = new Emprestimo(c2, l1);
        Emprestimo e3 = new Emprestimo(c3, l3);
        Emprestimo e4 = new Emprestimo(c4, l4);
        Emprestimo e5 = new Emprestimo(c5, l5);
        Emprestimo e6 = new Emprestimo(c6, l1);
        Emprestimo e7 = new Emprestimo(c2, l7);
        Emprestimo e8 = new Emprestimo(c5, l5);
        Emprestimo e9 = new Emprestimo(c1, l4);
        Emprestimo e10 = new Emprestimo(c6, l3);

        controleDeEmprestimo.realizarEmprestimo(e1);
        controleDeEmprestimo.realizarEmprestimo(e2);
        controleDeEmprestimo.realizarEmprestimo(e3);
        controleDeEmprestimo.realizarEmprestimo(e4);
        controleDeEmprestimo.realizarEmprestimo(e5);
        controleDeEmprestimo.realizarEmprestimo(e6);
        controleDeEmprestimo.realizarEmprestimo(e7);
        controleDeEmprestimo.realizarEmprestimo(e8);
        controleDeEmprestimo.realizarEmprestimo(e9);
        controleDeEmprestimo.realizarEmprestimo(e10);

        controleDeEmprestimo.registrarDevolucao(e10);
        controleDeEmprestimo.registrarDevolucao(e8);
        controleDeEmprestimo.registrarDevolucao(e9);

        controleDeEspera.adicionarAFila(c3, l9);
        controleDeEspera.adicionarAFila(c6, l9);
        controleDeEspera.adicionarAFila(c1, l9);
    }

    @Override
    public void destroy() {
    }
}
