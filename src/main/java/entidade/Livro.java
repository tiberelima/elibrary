package entidade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

public class Livro implements Comparable<Livro>, Serializable {

    private String ISBN;
    private String titulo;
    private String autor;
    private String editora;
    private String genero;
    private String ano;
    private String edicao;
    private List<Exemplar> exemplares;
    private Queue<Cliente> espera;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getEdicao() {
        return edicao;
    }

    public void setEdicao(String edicao) {
        this.edicao = edicao;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public List<Exemplar> getExemplares() {
        if (exemplares == null) {
            exemplares = new ArrayList<>();
        }
        return exemplares;
    }

    public void setExemplares(List<Exemplar> exemplares) {
        this.exemplares = exemplares;
    }

    public Queue<Cliente> getEspera() {
        if (espera == null) {
            espera = new LinkedList<>();
        }
        return espera;
    }

    public void setEspera(Queue<Cliente> espera) {
        this.espera = espera;
    }

    @Override
    public int compareTo(Livro outro) {
        if (this == outro) {
            return 0;
        }
        if (outro == null) {
            return 1;
        }
        int comparacao = this.ISBN != null && outro.ISBN != null
                ? this.ISBN.compareTo(outro.ISBN)
                : this.ISBN != null ? 1 : -1;
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.titulo.compareTo(outro.titulo);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.autor.compareTo(outro.autor);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.editora.compareTo(outro.editora);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.genero.compareTo(outro.genero);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.ano.compareTo(outro.ano);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.edicao.compareTo(outro.edicao);
        if (comparacao != 0) {
            return comparacao;
        }
        assert this.equals(outro) : "compareTo inconsistente com equals.";
        return 0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.ISBN);
        hash = 73 * hash + Objects.hashCode(this.titulo);
        hash = 73 * hash + Objects.hashCode(this.autor);
        hash = 73 * hash + Objects.hashCode(this.editora);
        hash = 73 * hash + Objects.hashCode(this.genero);
        hash = 73 * hash + Objects.hashCode(this.ano);
        hash = 73 * hash + Objects.hashCode(this.edicao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Livro other = (Livro) obj;
        if (!Objects.equals(this.ISBN, other.ISBN)) {
            return false;
        }
        if (!Objects.equals(this.titulo, other.titulo)) {
            return false;
        }
        if (!Objects.equals(this.autor, other.autor)) {
            return false;
        }
        if (!Objects.equals(this.editora, other.editora)) {
            return false;
        }
        if (!Objects.equals(this.genero, other.genero)) {
            return false;
        }
        if (!Objects.equals(this.ano, other.ano)) {
            return false;
        }
        return Objects.equals(this.edicao, other.edicao);
    }

    @Override
    public String toString() {
        return "Livro{" + "ISBN=" + ISBN + ", titulo=" + titulo + ", autor=" + autor + ", editora=" + editora + ", genero=" + genero + ", ano=" + ano + ", edicao=" + edicao + ", exemplares=" + exemplares + ", espera=" + espera + '}';
    }

    public boolean temExemplarDisponivel() {
        if (!exemplares.isEmpty()) {
            for (Exemplar exemplar : exemplares) {
                if (exemplar.getEstado().equals(Estado.DISPONIVEL)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Exemplar proximoExemplarDisponivel() {
        for (Exemplar exemplar : exemplares) {
            if (exemplar.getEstado().equals(Estado.DISPONIVEL)) {
                return exemplar;
            }
        }
        return null;
    }

    public int getTotalDeExemplaresIndisponiveis() {
        int total = 0;
        for (Exemplar exemplar : exemplares) {
            if (!exemplar.getEstado().equals(Estado.DISPONIVEL)) {
                total++;
            }
        }
        return total;
    }

    public boolean isValido() {
        return ISBN != null
                && !ISBN.isEmpty()
                && !titulo.isEmpty()
                && !autor.isEmpty()
                && !editora.isEmpty()
                && !genero.isEmpty()
                && !ano.isEmpty()
                && !edicao.isEmpty();
    }

    public Livro copiarDe(Livro l) {
        this.setAno(l.getAno());
        this.setAutor(l.getAutor());
        this.setEdicao(l.getEdicao());
        this.setEditora(l.getEditora());
        this.setGenero(l.getGenero());
        this.setISBN(l.getISBN());
        this.setTitulo(l.getTitulo());
        this.setExemplares(l.getExemplares());
        this.setEspera(l.getEspera());
        return this;
    }
}
