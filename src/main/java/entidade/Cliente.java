package entidade;

import java.io.Serializable;
import java.util.Objects;

public class Cliente implements Comparable<Cliente>, Serializable {

    private String nome;
    private String rg;
    private String email;
    private String telefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public boolean validado() {
        return !nome.isEmpty() && !rg.isEmpty() && !email.isEmpty() && !telefone.isEmpty();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.nome);
        hash = 83 * hash + Objects.hashCode(this.rg);
        hash = 83 * hash + Objects.hashCode(this.email);
        hash = 83 * hash + Objects.hashCode(this.telefone);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.rg, other.rg)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return Objects.equals(this.telefone, other.telefone);
    }

    @Override
    public int compareTo(Cliente outro) {
        if (this == outro) {
            return 0;
        }
        int comparacao = this.nome.compareTo(outro.nome);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.email.compareTo(outro.email);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.rg.compareTo(outro.rg);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.telefone.compareTo(outro.telefone);
        if (comparacao != 0) {
            return comparacao;
        }
        assert this.equals(outro) : "compareTo inconsistente com equals.";
        return 0;
    }

    @Override
    public String toString() {
        return "Cliente{" + "nome=" + nome + ", rg=" + rg + ", email=" + email + ", telefone=" + telefone + '}';
    }

}
