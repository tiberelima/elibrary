package entidade;

import java.io.Serializable;
import java.util.Objects;

public class Usuario implements Comparable<Usuario>, Serializable {

    private String login;
    private String senha;
    private Nivel nivel;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    @Override
    public int compareTo(Usuario outro) {
        if (this == outro) {
            return 0;
        }
        int comparacao = this.login.compareTo(outro.login);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.senha.compareTo(outro.senha);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.nivel.compareTo(outro.nivel);
        if (comparacao != 0) {
            return comparacao;
        }
        assert this.equals(outro) : "compareTo inconsistente com equals.";
        return 0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.login);
        hash = 23 * hash + Objects.hashCode(this.senha);
        hash = 23 * hash + Objects.hashCode(this.nivel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        if (!Objects.equals(this.senha, other.senha)) {
            return false;
        }
        return this.nivel == other.nivel;
    }

}
