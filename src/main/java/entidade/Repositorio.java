package entidade;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

public class Repositorio {

    private NavigableSet<Livro> livros;
    private List<Emprestimo> emprestimo;
    private Deque<Emprestimo> devolucao;
    private static Repositorio instancia;

    private Repositorio() {
    }

    public static Repositorio getInstancia() {
        if (instancia == null) {
            instancia = new Repositorio();
        }
        return instancia;
    }

    public NavigableSet<Livro> livros() {
        if (livros == null) {
            livros = new TreeSet<>();
        }
        return livros;
    }

    public List<Emprestimo> emprestimo() {
        if (emprestimo == null) {
            emprestimo = new ArrayList<>();
        }
        return emprestimo;
    }

    public Deque<Emprestimo> devolucao() {
        if (devolucao == null) {
            devolucao = new ArrayDeque<>();
        }
        return devolucao;
    }
}
