package entidade;

import java.io.Serializable;
import java.util.Objects;

public class Exemplar implements Comparable<Exemplar>, Serializable {

    private Estado estado;

    public Exemplar(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.estado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Exemplar other = (Exemplar) obj;
        return this.estado == other.estado;
    }

    @Override
    public String toString() {
        return "Exemplar{" + "estado=" + estado + '}';
    }

    @Override
    public int compareTo(Exemplar outro) {
        if (this == outro) {
            return 0;
        }
        int comparacao = this.estado.compareTo(outro.estado);
        if (comparacao != 0) {
            return comparacao;
        }
        assert this.equals(outro) : "compareTo inconsistente com equals.";
        return 0;
    }

}
