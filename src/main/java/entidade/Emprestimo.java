package entidade;

import java.io.Serializable;

public class Emprestimo implements Comparable<Emprestimo>, Serializable {

    private Cliente cliente;
    private Livro livro;
    private Exemplar exemplar;

    public Emprestimo(Cliente cliente, Livro livro) {
        this.cliente = cliente;
        this.livro = livro;
    }

    public Exemplar getExemplar() {
        return exemplar;
    }

    public void setExemplar(Exemplar exemplar) {
        this.exemplar = exemplar;
    }

    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public int compareTo(Emprestimo outro) {
        if (this == outro) {
            return 0;
        }
        int comparacao = this.exemplar.compareTo(outro.exemplar);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.livro.compareTo(outro.livro);
        if (comparacao != 0) {
            return comparacao;
        }
        comparacao = this.cliente.compareTo(outro.cliente);
        if (comparacao != 0) {
            return comparacao;
        }
        assert this.equals(outro) : "compareTo inconsistente com equals.";
        return 0;
    }

}
