package fronteira;

import controle.ControleDeAcesso;
import entidade.Usuario;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "acesso")
@SessionScoped
public class AcessoMB implements Serializable {

    private final Usuario usuario;
    private boolean logado;

    public AcessoMB() {
        usuario = new Usuario();
    }

    public void entrar() {
        logado = new ControleDeAcesso().validarAcesso(usuario);
        if (logado) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Bem vindo!", "Bem vindo!"));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuário ou senha incorreto", "Usuário ou senha incorreto"));
        }
    }

    public void sair() {
        logado = false;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public boolean isLogado() {
        return logado;
    }

}
