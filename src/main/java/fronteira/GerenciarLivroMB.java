package fronteira;

import controle.ControleDeEmprestimo;
import controle.ControleDeEspera;
import controle.ControleDeExemplares;
import controle.ControleDeLivro;
import entidade.Cliente;
import entidade.Emprestimo;
import entidade.Livro;
import entidade.Repositorio;
import java.io.Serializable;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "gerenciarLivro")
@SessionScoped
public class GerenciarLivroMB implements Serializable {

    private final ControleDeLivro controleDeLivro;
    private final ControleDeEspera controleDeEspera;
    private final ControleDeEmprestimo controleDeEmprestimo;
    private final ControleDeExemplares controleDeExemplares;
    private List<Livro> resultadoDaPesquisa;
    private Livro livroAtual;
    private Livro livroAntigo;
    private Cliente cliente;
    private int numeroExemplares;
    private String ISBN;
    private String titulo;
    private String autor;
    private String editora;
    private String genero;
    private String ano;
    private String edicao;
    private String rg;
    private String nome;
    private String email;
    private String telefone;

    public GerenciarLivroMB() {
        controleDeLivro = new ControleDeLivro();
        controleDeEspera = new ControleDeEspera();
        controleDeEmprestimo = new ControleDeEmprestimo();
        controleDeExemplares = new ControleDeExemplares();
    }

    public void adicionarLivro() {
        if (controleDeLivro.adicionar(construirLivro())) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Livro cadastrado", "Livro cadastrado"));
            limparDados();
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public void removerLivro(Livro livro) {
        if (controleDeLivro.remover(livro)) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Livro removido", "Livro removido"));
            resultadoDaPesquisa.remove(livro);
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public String editarLivro(Livro livro) {
        this.livroAntigo = new Livro().copiarDe(livro);
        this.livroAtual = new Livro().copiarDe(livro);
        numeroExemplares = livro.getExemplares().size();
        return "editar";
    }

    public void atualizar() {
        controleDeExemplares.alterarTotalDeExemplares(livroAtual, numeroExemplares);
        if (controleDeLivro.atualizar(livroAntigo, livroAtual)) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Livro atualizado", "Livro atualizado"));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public String filaDeEspera(Livro livro) {
        this.livroAtual = new Livro().copiarDe(livro);
        limparDados();
        return "espera";
    }

    public void registrarEmprestimo() {
        Emprestimo emprestimo = new Emprestimo(livroAtual.getEspera().peek(), livroAtual);
        if (controleDeEmprestimo.realizarEmprestimo(emprestimo)) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Empréstimo registrado", "Empréstimo registrado"));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public void adicionarAFila() {
        Cliente c = new Cliente();
        c.setRg(rg);
        c.setNome(nome);
        c.setEmail(email);
        c.setTelefone(telefone);
        if (controleDeEspera.adicionarAFila(c, livroAtual)) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente adicionado à fila", "Cliente adicionado à fila"));
            limparDados();
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public void registrarDevolucao(Emprestimo emprestimo) {
        if (controleDeEmprestimo.registrarDevolucao(emprestimo)) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Devolução registrada", "Devolução registrada"));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public void disponibilizarExemplar() {
        if (controleDeEmprestimo.disponibilizarExemplar()) {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Exemplar disponível", "Exemplar disponível"));
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_WARN, "Falha", "Falha"));
        }
    }

    public Collection<Livro> getLivros() {
        return Repositorio.getInstancia().livros();
    }

    public List<Emprestimo> getEmEmprestimo() {
        return Repositorio.getInstancia().emprestimo();
    }

    public Deque<Emprestimo> getDevolvido() {
        return Repositorio.getInstancia().devolucao();
    }

    public Collection<Livro> getResultadoDaPesquisa() {
        return resultadoDaPesquisa;
    }

    public void pesquisarLivro() {
        if (resultadoDaPesquisa != null) {
            resultadoDaPesquisa.clear();
        }
        resultadoDaPesquisa = controleDeLivro.pesquisar(construirLivro());
        limparDados();
    }

    private void limparDados() {
        ISBN = "";
        titulo = "";
        autor = "";
        editora = "";
        genero = "";
        ano = "";
        edicao = "";
        rg = "";
        nome = "";
        email = "";
        telefone = "";
    }

    private Livro construirLivro() {
        Livro l = new Livro();
        if (ISBN != null && !ISBN.isEmpty()) {
            l.setISBN(ISBN);
        }
        if (titulo != null && !titulo.isEmpty()) {
            l.setTitulo(titulo);
        }
        if (autor != null && !autor.isEmpty()) {
            l.setAutor(autor);
        }
        if (editora != null && !editora.isEmpty()) {
            l.setEditora(editora);
        }
        if (genero != null && !genero.isEmpty()) {
            l.setGenero(genero);
        }
        if (ano != null && !ano.isEmpty()) {
            l.setAno(ano);
        }
        if (edicao != null && !edicao.isEmpty()) {
            l.setEdicao(edicao);
        }
        return l;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getEdicao() {
        return edicao;
    }

    public void setEdicao(String edicao) {
        this.edicao = edicao;
    }

    public int getNumeroExemplares() {
        return numeroExemplares;
    }

    public void setNumeroExemplares(int numeroExemplares) {
        this.numeroExemplares = numeroExemplares;
    }

    public void setResultadoDaPesquisa(List<Livro> resultadoDaPesquisa) {
        this.resultadoDaPesquisa = resultadoDaPesquisa;
    }

    public Livro getLivro() {
        return livroAtual;
    }

    public void setLivro(Livro livro) {
        this.livroAtual = livro;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

}
