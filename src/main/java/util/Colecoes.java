package util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Colecoes {

    public static <T> void filtrar(Collection<T> collection, Condicao<T> condicao) {
        if ((collection != null) && (condicao != null)) {
            Iterator<T> itr = collection.iterator();
            while (itr.hasNext()) {
                if (!condicao.teste(itr.next())) {
                    itr.remove();
                }
            }
        }
    }

    public static <T> void filtrar(Collection<T> collection, List<Condicao<T>> condicoes) {
        for (Condicao<T> condicao : condicoes) {
            filtrar(collection, condicao);
        }
    }
}
