package util;

public interface Condicao<T> {

    boolean teste(T o);

}
