package controle;

import entidade.Estado;
import entidade.Exemplar;
import entidade.Livro;
import java.io.Serializable;

public class ControleDeExemplares implements Serializable {

    public void adicionarExemplares(Livro livro, int quantidade) {
        for (int i = 0; i < quantidade; i++) {
            livro.getExemplares().add(new Exemplar(Estado.DISPONIVEL));
        }
    }

    public void removerExemplares(Livro livro, int quantidade) {
        int quantidadeAtual = livro.getExemplares().size();
        if (quantidade <= quantidadeAtual) {
            for (int i = 0; i < quantidade; i++) {
                for (Exemplar exemplar : livro.getExemplares()) {
                    if (exemplar.getEstado().equals(Estado.DISPONIVEL)) {
                        livro.getExemplares().remove(exemplar);
                        break;
                    }
                }
            }
        }
    }

    public void alterarTotalDeExemplares(Livro livro, int novaQuantidade) {
        int quantidadeAtual = livro.getExemplares().size();
        if (novaQuantidade > quantidadeAtual) {
            adicionarExemplares(livro, novaQuantidade - quantidadeAtual);
            return;
        }
        if (novaQuantidade < quantidadeAtual) {
            removerExemplares(livro, quantidadeAtual - novaQuantidade);
        }
    }

}
