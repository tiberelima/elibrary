package controle;

import entidade.Cliente;
import entidade.Livro;
import entidade.Repositorio;
import java.io.Serializable;
import java.util.Queue;

public class ControleDeEspera implements Serializable {

    public boolean adicionarAFila(Cliente cliente, Livro livro) {
        if (Repositorio.getInstancia().livros().contains(livro)) {
            Queue<Cliente> espera = Repositorio.getInstancia().livros().ceiling(livro).getEspera();
            if (!espera.contains(cliente)) {
                return espera.offer(cliente);
            }
        }
        return false;
    }

    public void removerDaFila(Livro livro) {
        if (Repositorio.getInstancia().livros().contains(livro)) {
            Repositorio.getInstancia().livros().ceiling(livro).getEspera().poll();
        }
    }

}
