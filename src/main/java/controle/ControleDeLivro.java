package controle;

import static util.Colecoes.filtrar;
import util.Condicao;
import entidade.Livro;
import entidade.Repositorio;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ControleDeLivro implements Serializable {

    public boolean adicionar(Livro livro) {
        if (livro != null && livro.isValido()) {
            Livro filtroISBN = new Livro();
            filtroISBN.setISBN(livro.getISBN());
            if (pesquisar(filtroISBN).size() < 1) {
                return Repositorio.getInstancia().livros().add(livro);
            }
        }
        return false;
    }

    public boolean remover(Livro livro) {
        if (livro.getExemplares().isEmpty() || livro.getTotalDeExemplaresIndisponiveis() == 0) {
            return Repositorio.getInstancia().livros().remove(livro);
        }
        return false;
    }

    public boolean atualizar(Livro antigo, Livro novo) {
        if (antigo.getISBN().equals(novo.getISBN())) {
            if (remover(antigo)) {
                return adicionar(novo);
            }
            return false;
        }
        Livro filtroISBN = new Livro();
        filtroISBN.setISBN(novo.getISBN());
        if (pesquisar(filtroISBN).size() < 1) {
            if (remover(antigo)) {
                return adicionar(novo);
            }
        }
        return false;
    }

    public List<Livro> pesquisar(final Livro criterios) {

        List<Livro> resultado = new ArrayList<>(Repositorio.getInstancia().livros());

        filtrar(resultado, new Condicao<Livro>() {
            @Override
            public boolean teste(Livro l) {
                return criterios.getISBN() != null && l.getISBN().toLowerCase().contains(criterios.getISBN().toLowerCase())
                        || criterios.getAno() != null && l.getAno().toLowerCase().contains(criterios.getAno().toLowerCase())
                        || criterios.getAutor() != null && l.getAutor().toLowerCase().contains(criterios.getAutor().toLowerCase())
                        || criterios.getTitulo() != null && l.getTitulo().toLowerCase().contains(criterios.getTitulo().toLowerCase())
                        || criterios.getEdicao() != null && l.getEdicao().toLowerCase().contains(criterios.getEdicao().toLowerCase())
                        || criterios.getGenero() != null && l.getGenero().toLowerCase().contains(criterios.getGenero().toLowerCase())
                        || criterios.getEditora() != null && l.getEditora().toLowerCase().contains(criterios.getEditora().toLowerCase());
            }
        });
        return resultado;
    }

}
