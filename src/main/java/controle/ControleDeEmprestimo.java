package controle;

import entidade.Emprestimo;
import entidade.Estado;
import entidade.Exemplar;
import entidade.Repositorio;
import java.io.Serializable;

public class ControleDeEmprestimo implements Serializable {

    public boolean realizarEmprestimo(Emprestimo emprestimo) {
        if (emprestimo.getCliente().validado() && emprestimo.getLivro().temExemplarDisponivel()) {
            emprestimo.getLivro().getEspera().poll();
            Exemplar exemplar = emprestimo.getLivro().proximoExemplarDisponivel();
            emprestimo.getLivro().getExemplares().remove(exemplar);
            exemplar.setEstado(Estado.EMPRESTADO);
            emprestimo.getLivro().getExemplares().add(exemplar);
            emprestimo.setExemplar(exemplar);
            return Repositorio.getInstancia().emprestimo().add(emprestimo);
        }
        return false;
    }

    public boolean registrarDevolucao(Emprestimo emprestimo) {
        if (emprestimo.getExemplar().getEstado().equals(Estado.EMPRESTADO)) {
            Repositorio.getInstancia().emprestimo().remove(emprestimo);
            emprestimo.getExemplar().setEstado(Estado.DEVOLVIDO);
            return Repositorio.getInstancia().devolucao().offerFirst(emprestimo);
        }
        return false;
    }

    public boolean disponibilizarExemplar() {
        Emprestimo emprestimo = Repositorio.getInstancia().devolucao().pollFirst();
        if (emprestimo != null) {
            emprestimo.getExemplar().setEstado(Estado.DISPONIVEL);
            return true;
        }
        return false;
    }
}
