package controle;

import entidade.Nivel;
import entidade.Usuario;

public class ControleDeAcesso {

    public boolean validarAcesso(Usuario usuario) {
        Usuario bibliotecario = new Usuario();
        bibliotecario.setLogin("admin");
        bibliotecario.setSenha("admin");
        bibliotecario.setNivel(Nivel.BIBLIOTECARIO);
        usuario.setNivel(Nivel.BIBLIOTECARIO);
        return usuario.equals(bibliotecario);
    }

}
