package com.tlima.modelo;

import entidade.Repositorio;
import org.junit.Test;
import static org.junit.Assert.*;

public class RepositorioTest {

    @Test
    public void testInicializarLivros() {
        assertNotNull(Repositorio.getInstancia().livros());
    }

    @Test
    public void testInicializarDevolucao() {
        assertNotNull(Repositorio.getInstancia().devolucao());
    }

    @Test
    public void testInicializarEmprestimo() {
        assertNotNull(Repositorio.getInstancia().emprestimo());
    }

}
