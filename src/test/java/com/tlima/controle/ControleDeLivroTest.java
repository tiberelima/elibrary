package com.tlima.controle;

import controle.ControleDeExemplares;
import controle.ControleDeLivro;
import entidade.Cliente;
import entidade.Estado;
import entidade.Livro;
import entidade.Repositorio;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class ControleDeLivroTest {

    @Before
    public void setup() {
        Repositorio.getInstancia().livros().clear();
    }

    @Test
    public void testAdicionar1Livro() {
        Livro livro = criarNovoLivro();
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        assertEquals(1, Repositorio.getInstancia().livros().size());
    }

    @Test
    public void testAdicionar2LivrosDiferentes() {
        Livro livro = criarNovoLivro();
        Livro livro2 = criarLivro();
        livro2.setISBN("2222222222");
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        instance.adicionar(livro2);
        assertEquals(2, Repositorio.getInstancia().livros().size());
    }

    @Test
    public void testNaoConseguirAdicionar2LivrosIguais() {
        Livro livro = criarNovoLivro();
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        instance.adicionar(livro);
        assertEquals(1, Repositorio.getInstancia().livros().size());
    }

    @Test
    public void testRemoverLivro() {
        Livro livro = criarNovoLivro();
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        instance.remover(livro);
        assertEquals(0, Repositorio.getInstancia().livros().size());
    }

    @Test
    public void testNaoRemoverLivroComExemplarIndisponivel() {
        Livro livro = criarNovoLivro();
        ControleDeLivro controleDeLivro = new ControleDeLivro();
        controleDeLivro.adicionar(livro);

        new ControleDeExemplares().adicionarExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());

        livro.getExemplares().get(0).setEstado(Estado.DEVOLVIDO);

        controleDeLivro.remover(livro);
        assertEquals(1, Repositorio.getInstancia().livros().size());
    }

    @Test
    public void testAtualizarLivro() {
        String isbn = String.valueOf(new Date().getTime());
        Livro livro = criarLivro();
        livro.setISBN(isbn);
        livro.setTitulo("Livro 1");
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);

        Livro atualizado = criarLivro();
        atualizado.setISBN(isbn);
        atualizado.setTitulo("Livro 1 atualizado");
        instance.atualizar(livro, atualizado);

        assertEquals(1, Repositorio.getInstancia().livros().size());
        Livro recuperado = ((TreeSet<Livro>) Repositorio.getInstancia().livros()).pollFirst();
        assertEquals(0, Repositorio.getInstancia().livros().size());
        assertEquals(isbn, recuperado.getISBN());
        assertEquals("Livro 1 atualizado", recuperado.getTitulo());
    }

    @Test
    public void testRemoverMesmoLivro() {
        Livro livro = criarNovoLivro();
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        instance.remover(livro);
        instance.remover(livro);
        instance.remover(livro);
        instance.remover(livro);
        assertEquals(0, Repositorio.getInstancia().livros().size());
    }

    @Test
    public void testAdicionarClienteAFilaDeEspera() {
        Livro livro = criarNovoLivro();
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        assertEquals(1, Repositorio.getInstancia().livros().size());

        Cliente cliente = criarClienteComId(1l);

        livro.getEspera().offer(cliente);

        assertEquals(1, livro.getEspera().size());
    }

    @Test
    public void testRemoverClienteDaFilaDeEspera() {
        Livro livro = criarNovoLivro();
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        assertEquals(1, Repositorio.getInstancia().livros().size());

        Cliente cliente = criarClienteComId(1l);

        livro.getEspera().offer(cliente);
        Cliente c = livro.getEspera().poll();

        assertEquals(cliente, c);
        assertEquals(0, livro.getEspera().size());
    }

    @Test
    public void testPesquisarLivroPeloTitulo() {

        ControleDeLivro controle = new ControleDeLivro();
        for (int i = 0; i < 10; i++) {
            Livro livro = criarNovoLivro();
            livro.setTitulo("Livro " + i);
            livro.setISBN("ISBN " + i);
            controle.adicionar(livro);
        }
        assertEquals(10, Repositorio.getInstancia().livros().size());
        Livro criterios = new Livro();
        criterios.setTitulo("Livro 3");

        Collection<Livro> resultado = controle.pesquisar(criterios);

        assertEquals(1, resultado.size());
        assertEquals("Livro 3", ((ArrayList<Livro>) resultado).get(0).getTitulo());
        assertEquals(10, Repositorio.getInstancia().livros().size());

    }

    @Test
    public void testPesquisarLivroPeloTituloEAutor() {
        int maxI = 4;
        int maxJ = 4;

        ControleDeLivro controle = new ControleDeLivro();
        for (int i = 0; i < maxI; i++) {
            for (int j = 0; j < maxJ; j++) {
                Livro livro = criarNovoLivro();
                livro.setTitulo("Livro " + i);
                livro.setAutor("Autor " + j);
                livro.setISBN("ISBN " + i + "-" + j);
                controle.adicionar(livro);
            }
        }
        assertEquals(maxI * maxJ, Repositorio.getInstancia().livros().size());
        Livro criterios = new Livro();
        criterios.setTitulo("Livro 3");
        criterios.setAutor("Autor 1");

        Collection<Livro> resultado = controle.pesquisar(criterios);

        for (Livro livro : resultado) {
            assertTrue("Livro 3".equals(livro.getTitulo()) || "Autor 1".equals(livro.getAutor()));
        }
        assertEquals(7, resultado.size());
        assertEquals(maxI * maxJ, Repositorio.getInstancia().livros().size());
    }

    private Cliente criarClienteComId(long id) {
        Cliente cliente = new Cliente();
        cliente.setNome("Cliente " + id);
        cliente.setRg("123.456.789-0");
        return cliente;
    }

    private Livro criarNovoLivro() {
        Livro livro = criarLivro();
        livro.setISBN(String.valueOf(new Date().getTime()));
        return livro;
    }

    private Livro criarLivro() {
        Livro livro = new Livro();
        livro.setTitulo("Livro para emprestimo");
        livro.setAno("ano");
        livro.setAutor("Autor");
        livro.setEdicao("edicao");
        livro.setEditora("editora");
        livro.setGenero("genero");
        livro.setISBN("isbn");
        return livro;
    }
}
