package com.tlima.controle;

import controle.ControleDeEmprestimo;
import entidade.Cliente;
import entidade.Emprestimo;
import entidade.Estado;
import entidade.Exemplar;
import entidade.Livro;
import entidade.Repositorio;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ControleDeEmprestimoTest {

    @Before
    public void setUp() {
        Repositorio.getInstancia().emprestimo().clear();
        Repositorio.getInstancia().devolucao().clear();
    }

    @Test
    public void testRealizarEmprestimoComExemplarDisponivel() {
        Cliente cliente = criarNovoCliente();

        Livro livro = criarLivro();
        List<Exemplar> exemplares = new ArrayList<>();
        exemplares.add(new Exemplar(Estado.DISPONIVEL));
        exemplares.add(new Exemplar(Estado.DEVOLVIDO));
        exemplares.add(new Exemplar(Estado.EMPRESTADO));

        livro.setExemplares(exemplares);

        ControleDeEmprestimo controle = new ControleDeEmprestimo();
        Emprestimo emprestimo = new Emprestimo(cliente, livro);
        controle.realizarEmprestimo(emprestimo);

        verifiqueCom(livro, Estado.DISPONIVEL);

        assertEquals(1, Repositorio.getInstancia().emprestimo().size());
    }

    @Test
    public void testRealizarEmprestimoSemExemplarDisponivel() {
        Cliente cliente = criarNovoCliente();

        List<Exemplar> exemplares = new ArrayList<>();
        exemplares.add(new Exemplar(Estado.EMPRESTADO));
        exemplares.add(new Exemplar(Estado.EMPRESTADO));

        Livro livro = criarNovoLivroCom(exemplares);

        ControleDeEmprestimo controle = new ControleDeEmprestimo();
        Emprestimo emprestimo = new Emprestimo(cliente, livro);
        controle.realizarEmprestimo(emprestimo);
        verifiqueCom(livro, Estado.DISPONIVEL);
        assertEquals(0, Repositorio.getInstancia().emprestimo().size());
    }

    @Test
    public void testRegistrarDevolucao() {
        Cliente cliente = criarNovoCliente();

        List<Exemplar> exemplares = new ArrayList<>();
        exemplares.add(new Exemplar(Estado.DISPONIVEL));
        exemplares.add(new Exemplar(Estado.DEVOLVIDO));
        exemplares.add(new Exemplar(Estado.EMPRESTADO));

        Livro livro = criarNovoLivroCom(exemplares);

        ControleDeEmprestimo controle = new ControleDeEmprestimo();
        Emprestimo emprestimo = new Emprestimo(cliente, livro);
        controle.realizarEmprestimo(emprestimo);
        Exemplar emprestado = null;
        for (Exemplar e : livro.getExemplares()) {
            assertFalse(e.getEstado().equals(Estado.DISPONIVEL));
            if (e.getEstado().equals(Estado.EMPRESTADO)) {
                emprestado = e;
                break;
            }
        }
        assertEquals(1, Repositorio.getInstancia().emprestimo().size());
        assertNotNull(emprestado);
        emprestimo.setExemplar(emprestado);
        controle.registrarDevolucao(emprestimo);
        assertEquals(0, Repositorio.getInstancia().emprestimo().size());
        assertEquals(1, Repositorio.getInstancia().devolucao().size());

    }

    @Test
    public void testDisponibilizarExemplarParaEmprestimo() {
        Cliente cliente = criarNovoCliente();

        List<Exemplar> exemplares = new ArrayList<>();
        exemplares.add(new Exemplar(Estado.DISPONIVEL));

        Livro livro = criarNovoLivroCom(exemplares);

        ControleDeEmprestimo controle = new ControleDeEmprestimo();
        Emprestimo emprestimo = new Emprestimo(cliente, livro);
        controle.realizarEmprestimo(emprestimo);
        Exemplar emprestado = null;
        for (Exemplar e : livro.getExemplares()) {
            assertFalse(e.getEstado().equals(Estado.DISPONIVEL));
            if (e.getEstado().equals(Estado.EMPRESTADO)) {
                emprestado = e;
                break;
            }
        }
        assertEquals(1, Repositorio.getInstancia().emprestimo().size());
        assertNotNull(emprestado);
        emprestimo.setExemplar(emprestado);
        controle.registrarDevolucao(emprestimo);
        assertEquals(0, Repositorio.getInstancia().emprestimo().size());
        assertEquals(1, Repositorio.getInstancia().devolucao().size());

        controle.disponibilizarExemplar();
        assertEquals(0, Repositorio.getInstancia().devolucao().size());
        assertEquals(1, livro.getExemplares().size());

        for (Exemplar e : livro.getExemplares()) {
            assertFalse(e.getEstado().equals(Estado.EMPRESTADO));
            assertFalse(e.getEstado().equals(Estado.DEVOLVIDO));
        }

    }

    private Cliente criarNovoCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("nome");
        cliente.setRg("rg");
        cliente.setEmail("email");
        cliente.setTelefone("telefone");
        return cliente;
    }

    private Livro criarNovoLivroCom(List<Exemplar> exemplares) {
        Livro livro = criarLivro();
        livro.setExemplares(exemplares);
        return livro;
    }

    private void verifiqueCom(Livro livro, Estado estado) {
        for (Exemplar e : livro.getExemplares()) {
            assertFalse(e.getEstado().equals(estado));
        }
    }

    private Livro criarLivro() {
        Livro livro = new Livro();
        livro.setTitulo("Livro para emprestimo");
        livro.setAno("ano");
        livro.setAutor("Autor");
        livro.setEdicao("edicao");
        livro.setEditora("editora");
        livro.setGenero("genero");
        livro.setISBN("isbn");
        return livro;
    }
}
