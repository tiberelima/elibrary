package com.tlima.controle;

import controle.ControleDeExemplares;
import entidade.Estado;
import entidade.Exemplar;
import entidade.Livro;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ControleDeExemplaresTest {

    private final ControleDeExemplares controle;

    public ControleDeExemplaresTest() {
        controle = new ControleDeExemplares();
    }

    @Before
    public void setUp() {
    }

    @Test
    public void testAdicionar1NovoExemplar() {
        Livro livro = new Livro();
        controle.adicionarExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());
    }

    @Test
    public void testAdicionar_N_NovosExemplares() {
        int total = 0, n;
        Livro livro = new Livro();

        n = 1;
        total += n;
        controle.adicionarExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());

        n = 2;
        total += n;
        controle.adicionarExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());

        n = 3;
        total += n;
        controle.adicionarExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());

        n = 4;
        total += n;
        controle.adicionarExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());
    }

    @Test
    public void testRemover1Exemplar() {
        Livro livro = new Livro();

        controle.adicionarExemplares(livro, 2);
        assertEquals(2, livro.getExemplares().size());

        controle.removerExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());
    }

    @Test
    public void testRemover1ExemplarComEstadoDiferenteDeDisponivel() {
        Livro livro = new Livro();

        controle.adicionarExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());

        for (Exemplar exemplar : livro.getExemplares()) {
            exemplar.setEstado(Estado.EMPRESTADO);
        }

        controle.removerExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());

        for (Exemplar exemplar : livro.getExemplares()) {
            exemplar.setEstado(Estado.DEVOLVIDO);
        }

        controle.removerExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());

        for (Exemplar exemplar : livro.getExemplares()) {
            exemplar.setEstado(Estado.DISPONIVEL);
        }

        controle.removerExemplares(livro, 1);
        assertEquals(0, livro.getExemplares().size());

    }

    @Test
    public void testRemoverNExemplarComEstadoDiferenteDeDisponivel() {
        Livro livro = new Livro();

        controle.adicionarExemplares(livro, 6);
        assertEquals(6, livro.getExemplares().size());

        livro.getExemplares().get(0).setEstado(Estado.DEVOLVIDO);
        livro.getExemplares().get(1).setEstado(Estado.DEVOLVIDO);
        livro.getExemplares().get(2).setEstado(Estado.EMPRESTADO);
        livro.getExemplares().get(3).setEstado(Estado.DEVOLVIDO);
        livro.getExemplares().get(4).setEstado(Estado.EMPRESTADO);
        livro.getExemplares().get(5).setEstado(Estado.EMPRESTADO);

        controle.removerExemplares(livro, 3);
        assertEquals(6, livro.getExemplares().size());

    }

    @Test
    public void testRemoverNExemplarComEstadoDisponivel() {
        Livro livro = new Livro();

        controle.adicionarExemplares(livro, 6);
        assertEquals(6, livro.getExemplares().size());

        livro.getExemplares().get(0).setEstado(Estado.DISPONIVEL);
        livro.getExemplares().get(1).setEstado(Estado.DISPONIVEL);
        livro.getExemplares().get(2).setEstado(Estado.DISPONIVEL);
        livro.getExemplares().get(3).setEstado(Estado.DISPONIVEL);
        livro.getExemplares().get(4).setEstado(Estado.DISPONIVEL);
        livro.getExemplares().get(5).setEstado(Estado.DISPONIVEL);

        controle.removerExemplares(livro, 3);
        assertEquals(3, livro.getExemplares().size());

    }

    @Test
    public void testRemover_N_Exemplares() {
        int total = 10, n;
        Livro livro = new Livro();
        controle.adicionarExemplares(livro, total);
        assertEquals(total, livro.getExemplares().size());

        n = 1;
        total -= n;
        controle.removerExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());

        n = 2;
        total -= n;
        controle.removerExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());

        n = 3;
        total -= n;
        controle.removerExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());

        n = 4;
        total -= n;
        controle.removerExemplares(livro, n);
        assertEquals(total, livro.getExemplares().size());
    }

    @Test
    public void testAlterarTotalDeExemplaresDeFormaPositiva() {
        Livro livro = new Livro();
        controle.adicionarExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());

        controle.alterarTotalDeExemplares(livro, 10);
        assertEquals(10, livro.getExemplares().size());
    }

    @Test
    public void testAlterarTotalDeExemplaresDeFormaNegativa() {
        Livro livro = new Livro();
        controle.adicionarExemplares(livro, 10);
        assertEquals(10, livro.getExemplares().size());

        controle.alterarTotalDeExemplares(livro, 1);
        assertEquals(1, livro.getExemplares().size());
    }

}
