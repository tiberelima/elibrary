package com.tlima.controle;

import controle.ControleDeEspera;
import controle.ControleDeLivro;
import entidade.Cliente;
import entidade.Livro;
import entidade.Repositorio;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ControleDeEsperaTest {

    @Before
    public void setup() {
        Repositorio.getInstancia().livros().clear();
    }

    @Test
    public void testAdicionarAFila() {
        Livro livro = inserirUmNovoLivroNoRepositorio();

        Cliente cliente = criarUmNovoClienteComId(1l);
        Cliente cliente2 = criarUmNovoClienteComId(2l);

        ControleDeEspera controleDeEspera = new ControleDeEspera();
        controleDeEspera.adicionarAFila(cliente, livro);
        controleDeEspera.adicionarAFila(cliente2, livro);

        assertEquals(2, livro.getEspera().size());
    }

    @Test
    public void testRemoverDaFila() {
        Livro livro = inserirUmNovoLivroNoRepositorio();

        Cliente cliente = criarUmNovoClienteComId(1l);

        ControleDeEspera controleDeEspera = new ControleDeEspera();
        controleDeEspera.adicionarAFila(cliente, livro);
        assertEquals(1, livro.getEspera().size());
        controleDeEspera.removerDaFila(livro);
        assertEquals(0, livro.getEspera().size());

    }

    private Cliente criarUmNovoClienteComId(long id) {
        Cliente cliente = new Cliente();
        cliente.setNome("Cliente " + id);
        cliente.setRg("123.456.789-0");
        return cliente;
    }

    private Livro inserirUmNovoLivroNoRepositorio() {
        Livro livro = criarLivro();
        livro.setISBN(String.valueOf(new Date().getTime()));
        ControleDeLivro instance = new ControleDeLivro();
        instance.adicionar(livro);
        assertEquals(1, Repositorio.getInstancia().livros().size());
        return livro;
    }

    private Livro criarLivro() {
        Livro livro = new Livro();
        livro.setTitulo("Livro para emprestimo");
        livro.setAno("ano");
        livro.setAutor("Autor");
        livro.setEdicao("edicao");
        livro.setEditora("editora");
        livro.setGenero("genero");
        livro.setISBN("isbn");
        return livro;
    }

}
